import { LitElement, html } from 'lit';

class PersonaDM extends LitElement{
    
    static get properties(){
        return{
            people: {type: Array}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Hulk Hogan",
                yearsInCompany: 10,
                photo: {
                    src: "./img/HkHogan.jpg",
                    alt: "Hulk Hogan"
                },
                profile: "Lucha libre"
            },{
                name: "Mr.T",
                yearsInCompany: 2,
                photo: {
                    src: "./img/MrT.JPG",
                    alt: "Mr.T"
                },
                profile: "Lucha grecorromana"
            },{
                name: "Rambo",
                yearsInCompany: 5,
                photo: {
                    src: "./img/Rambo2.jpg",
                    alt: "Rambo"
                },
                profile: "Mercenario"
            },{
                name: "Chuck Norris",
                yearsInCompany: 9,
                photo: {
                    src: "./img/CNorris.jpg",
                    alt: "Chuck Norris"
                },
                profile: "Policia"
            },{
                name: "Drago",
                yearsInCompany: 1,
                photo: {
                    src: "./img/Drago.jpg",
                    alt: "Drago"
                },
                profile: "Boxeo"
            }
        ]

    }
    
    
    render(){
        return html`
            <h1>Persona-dm</h1>
        `
    }

    updated(changedProperties){
        console.log("updated persona-dm");

        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad updated persona-dm");

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            )


        }

    }
}

customElements.define("persona-dm",PersonaDM);