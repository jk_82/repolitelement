
import { LitElement, html } from 'lit';

class TestApi extends LitElement{
    
    static get properties(){
        return{
            movies: {type: Array}
        };
    }

    constructor(){
        super();

        this.movies = [];
        this.getMovieData();
    }
    
    render(){
        return html`
            ${this.movies.map(
                movie => html`
                    <div>La película ${movie.title} fue dirigida por ${movie.director}</div>
                `
            )}
        `
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");

        let xhr = new XMLHttpRequest();

        //La ejecucion es asincrona
        xhr.onload = () => {
            //Por defecto es un 200 si ha ido bien
            if(xhr.status === 200){
                console.log("Petición completada con éxito");

                let APIResponse = JSON.parse(xhr.responseText);

                //results no es una palabra reservada, ni metodo...
                //sino que lo vemos en la especificacion de la API
                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send();

        console.log("Fin de getMovieData");
    }
}

customElements.define("test-api",TestApi);