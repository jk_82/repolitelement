
import { LitElement, html } from 'lit';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement{
    
    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }
    
    
    render(){
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `
    }

    processEvent(e){
        console.log("Capurado evento del emisor");
        console.log(e);

        //this.shadowRoot es el dom local, como el document
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
    }
}

customElements.define("gestor-evento",GestorEvento);